module Main where

import           Control.Monad
import           Data.List          (isPrefixOf)
import qualified Data.Map           as M
import           Data.Maybe
import           System.Environment
import           System.IO

type Sudoku = [[Int]]
type Tbl = M.Map (Int, Int) Int

parseResult :: Handle -> IO (Maybe Sudoku)
parseResult h = do
  content <- hGetContents h
  return $ fmap toSudoku $ foldr procLine (Just M.empty) $ lines content
  where
    procLine :: String -> Maybe Tbl -> Maybe Tbl
    procLine l m | "s UNSATISFIABLE" `isPrefixOf` l = Nothing
    procLine ('v':l) m =
      let ps :: [Int]
          ps = filter (>0) $ map read $ words l
      in (\mp -> foldr (uncurry M.insert . decP) mp ps) <$> m
         where
           decP p = let (i', r) = divMod (p - 1) 81
                        (j', n) = divMod r        9
                    in ( (i'+1, j'+1),  n + 1)
    procLine l m = m

    toSudoku :: Tbl -> [[Int]]
    toSudoku mp =
      flip map [1..9] $ \i ->
      flip map [1..9] $ \j ->
      fromJust (M.lookup (i, j) mp)


-- parseResult :: Handle -> IO (Maybe Sudoku)
-- parseResult h = do
--   l <- hGetLine h
--   case l of
--     "UNSAT" ->
--       return Nothing
--     _ -> do
--       l <- hGetLine h
--       let ps = filter (>0) $ map read $ words l
--       let m = foldr (\n -> let (i',r) = divMod (n-1) 81
--                                (j',n') = divMod r     9
--                            in M.insert (i'+1,j'+1) (n'+1)) M.empty ps
--       return $ Just $
--         flip map [1..9] $ \i ->
--           flip map [1..9] $ \j ->
--             fromJust (M.lookup (i,j) m)

putSudoku :: Sudoku -> IO ()
putSudoku sudoku =
  forM_ sudoku $ \line ->
    putOneLine line
  where
    putOneLine []     = putStrLn ""
    putOneLine (n:ns) = do
      putStr $ (show n) ++ " "
      putOneLine ns

main :: IO ()
main = do
  fs0 <- getArgs
  let fs = case fs0 of { [] -> ["-"] ; _ -> fs0 }
  forM_ fs $ \f ->
    withFile' f $ \h -> do
      res <- parseResult h
      case res of
        Just s  -> putSudoku s >> putStrLn ""
        Nothing -> putStrLn $ "No solution for the file: <" ++ show f ++ ">."

  where
    withFile' "-" k = k stdin
    withFile' fp  k = withFile fp ReadMode k
