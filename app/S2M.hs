{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import           Control.Monad
import           System.Environment
import           System.IO

import           Control.Exception
import           System.Console.GetOpt

import           Data.Maybe


{-
This program takes an input file of the form
53_ _7_ ___
6__ 195 ___
_98 ___ _6_
8__ _6_ __3
4__ 8_3 __1
7__ _2_ __6
_6_ ___ 28_
___ 419 __5
___ _8_ _79
and generates a CNF to be solved by a SAT solver.
The inputs must contains exactly 9 lines.

We ignore puncuations "\t" " " "|" "-" "+" in parsing,
and treat non numbers as holes.
-}

type Number = Int -- where 0 is treated as a hole.

type Sudoku = [[Number]]

data Conf = Conf { inputFile  :: Maybe FilePath,
                   outputFile :: Maybe FilePath }

options :: [OptDescr (Conf -> Conf)]
options =
  [ Option "i" ["input"]  (ReqArg (\s c -> c { inputFile = Just s }) "FILENAME") "Input file.",
    Option "o" ["output"] (ReqArg (\s c -> c { outputFile = Just s }) "FILENAME") "Output file." ]

initConf :: Conf
initConf = Conf Nothing Nothing

parseArgs :: IO Conf
parseArgs = do
  prog <- getProgName
  args <- getArgs
  let header = "Usage: " ++ prog ++ " OPTIONS FILENAME"
  case getOpt RequireOrder options args of
    (os, rest, []) ->
       return $ foldr (.) id os (makeInit rest)
    (_,_,errs) ->
       error (concat errs ++ usageInfo header options)
   where
     makeInit (f:_) = initConf { inputFile = Just f }
     makeInit _     = initConf



hGetSudoku :: Handle -> IO Sudoku
hGetSudoku fh = parse 0 []
  where
    parse 9 ret = return $ reverse ret
    parse n ret = do
      b <- handle (\(e :: IOException) -> return Nothing) $ do
              l <- hGetLine fh
              return $ Just $ parseLine l
      case b of
        Nothing -> return (reverse ret)
        Just xs ->
          case xs of
            [] -> parse n ret
            _  -> parse (n+1) (take 9 (xs ++ repeat 0):ret)

    parseLine :: [Char] -> [Number]
    parseLine xs = go xs
      where
        go :: [Char] -> [Number]
        go [] = []
        go ('#':_) = []
        go (c:cs) | isSkippable c = go cs
        go (c:cs) | '0' <= c && c <= '9' =
          read ([c]) : go cs
        go (c:cs) = 0 : go cs

        isSkippable :: Char -> Bool
        isSkippable ' '  = True
        isSkippable '\t' = True
        isSkippable '|'  = True
        isSkippable '-'  = True
        isSkippable '+'  = True
        isSkippable _    = False

type PVar = Int
type CNF  = [[PVar]]

genCNF :: Sudoku -> CNF
genCNF board =
  [ [ enc i j n | n <- ns ] | i <- ns, j <- ns ] ++
  [ [ - enc i j n, - enc i j' n ] |
    i <- ns, j <- ns, j' <- [j+1..9], n <- ns ] ++
  [ [ - enc i j n, - enc i' j n ] |
    i <- ns, i' <- [i+1..9], j <- ns, n <- ns ] ++
  [ [ - enc i j n, - enc i' j' n ] |
    is <- blocks, js <- blocks,
    i <- is, i' <- is,
    j <- js, j' <- js, i /= i' && j /= j' && (i,j) < (i',j'),
    n <- ns ] ++
  [ [ enc i j n ] | i <- ns, j <- ns, let n = board !! (i-1) !! (j-1), n > 0 ]
  where
    blocks = [ [1,2,3], [4,5,6], [7,8,9] ]
    enc i j n = (i-1)*9^2 + (j-1)*9 + (n-1) + 1
    ns = [1..9]

hPutCNF :: Handle -> CNF -> IO ()
hPutCNF h cnf = do
  hPutStrLn h $ "p cnf " ++ show (9^3) ++ " " ++ show (length cnf)
  forM_ cnf (hPutClause h)
  where
    hPutClause h [] = hPutStrLn h "0"
    hPutClause h (n:ns) =
      hPutStr h (show n ++ " ") >> hPutClause h ns

main :: IO ()
main = do
  conf <- parseArgs
  s <- case inputFile conf of
         Nothing -> hGetSudoku stdin
         Just f  -> withFile f ReadMode hGetSudoku
  let cnf = genCNF s

  case outputFile conf of
    Nothing ->
      hPutCNF stdout cnf
    Just f ->
      withFile f WriteMode $ \h -> hPutCNF h (genCNF s)
