# Sudoku Solver by using MiniSAT intended to be used in a lecture

## Build 

This project is managed by Stack, so simply: 

    stack build
    
## How to Use 

Using `stack exec` is fine, but we provide shell-script wrappers for
convenience.

  * `encode-to-sat.sh`
  * `decode-from-sat.sh`
  
The names explain what they do. 

Specifically, 

    ./encode-to-sat.sh -i FILENAME > PROP.cnf
    
produces a CNF corresponding to the input to the standard output. 

Assuming that you have installed `minisat`, then you can solve
the CNF by:

    minisat PROP.cnf PROP.ans 
    
Then, we can convert the minisat's output to the corresponding sudoku
solution by:

    ./decode-from-sat.sh PROP.ans
